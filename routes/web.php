<?php

use App\Http\Controllers\Web\Admin\CategoryController as AdminCategoryController;
use App\Http\Controllers\Web\Admin\ProfileController as AdminProfileController;
use App\Http\Controllers\Web\Admin\PostController as AdminPostController;
use App\Http\Controllers\Web\Guest\PostController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [PostController::class, 'index']);

Route::post('/search', [PostController::class, 'search'])->name('search');

Route::get('/post/{slug}', [PostController::class, 'show'])->name('show');

Route::get('/category/{slug}', [PostController::class, 'category'])->name('category');

Route::get('/admin/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('admin.dashboard');

Route::get('/admin/profile', [AdminProfileController::class, 'index'])
    ->middleware(['auth'])
    ->name('admin.profile');

Route::put('/admin/profile/{id}', [AdminProfileController::class, 'update'])
    ->middleware(['auth'])
    ->name('admin.profile.update');

Route::get('/admin/category', [AdminCategoryController::class, 'index'])
    ->middleware(['auth'])
    ->name('admin.category');

Route::get('/admin/category/create', [AdminCategoryController::class, 'create'])
    ->middleware(['auth'])
    ->name('admin.category.create');

Route::post('/admin/category/create', [AdminCategoryController::class, 'store'])
    ->middleware(['auth'])
    ->name('admin.category.store');

Route::get('/admin/category/search', [AdminCategoryController::class, 'search'])
    ->middleware(['auth'])
    ->name('admin.category.search');

Route::get('/admin/category/edit/{id}', [AdminCategoryController::class, 'edit'])
    ->middleware(['auth'])
    ->name('admin.category.edit');

Route::put('/admin/category/edit/{id}', [AdminCategoryController::class, 'update'])
    ->middleware(['auth'])
    ->name('admin.category.update');

Route::delete('/admin/category/delete/{id}', [AdminCategoryController::class, 'destroy'])
    ->middleware(['auth'])
    ->name('admin.category.destroy');

Route::get('/admin/post', [AdminPostController::class, 'index'])
    ->middleware(['auth'])
    ->name('admin.post');

Route::get('/admin/post/create', [AdminPostController::class, 'create'])
    ->middleware(['auth'])
    ->name('admin.post.create');

Route::post('/admin/post/create', [AdminPostController::class, 'store'])
    ->middleware(['auth'])
    ->name('admin.post.store');

    Route::get('/admin/post/search', [AdminPostController::class, 'search'])
    ->middleware(['auth'])
    ->name('admin.post.search');

Route::get('/admin/post/edit/{id}', [AdminPostController::class, 'edit'])
    ->middleware(['auth'])
    ->name('admin.post.edit');

Route::put('/admin/post/edit/{id}', [AdminPostController::class, 'update'])
    ->middleware(['auth'])
    ->name('admin.post.update');

Route::delete('/admin/post/delete/{id}', [AdminPostController::class, 'destroy'])
    ->middleware(['auth'])
    ->name('admin.post.destroy');

require __DIR__ . '/auth.php';
