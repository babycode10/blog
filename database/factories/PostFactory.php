<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PostFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $title = $this->faker->sentence($nbWords = 6, $variableNbWords = true);

        return [
            'category_id' => Category::factory(),
            'title' => $title,
            'slug' => Str::slug($title, '-'),
            'short_description' => $this->faker->sentence($nbWords = 10, $variableNbWords = true),
            'content' => $this->faker->paragraph($nbSentences = 3, $variableNbSentences = true),
            'image' => '/storage/images/500',
            'thumbnail' => '/storage/images/245'
        ];
    }
}
