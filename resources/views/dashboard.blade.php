@extends('layouts.app')
@section('title', 'Dashboard')
@section('content')
    <div class="p-8">
        <div class="grid grid-cols-3 gap-4">
            <div class="bg-white overflow-hidden flex items-center border">
                <h3 class="h-14 w-14 flex items-center justify-center text-lg font-bold bg-gray-100 border m-2">{{ App\Models\Post::count() }}</h3>

                <div class="py-4 bg-white border-gray-200">

                    <p class="text-sm font-medium text-gray-600">{{ __('Post') }}</p>
                </div>
            </div>

            <div class="bg-white overflow-hidden flex items-center border">
                <h3 class="h-14 w-14 flex items-center justify-center text-lg font-bold bg-gray-100 border m-2">{{ App\Models\Category::count() }}</h3>

                <div class="py-4 bg-white border-gray-200">
                    <p class="text-sm font-medium text-gray-600">{{ __('Category') }}</p>
                </div>
            </div>

            <div class="bg-white overflow-hidden flex items-center border">
                <h3 class="h-14 w-14 flex items-center justify-center text-lg font-bold bg-gray-100 border m-2">{{ App\Models\User::count() }}</h3>

                <div class="py-4 bg-white border-gray-200">
                    <p class="text-sm font-medium text-gray-600">{{ __('User') }}</p>
                </div>
            </div>
        </div>
    </div>
@endsection