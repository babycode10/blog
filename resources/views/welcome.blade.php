@extends('layouts.guest')
@section('title', 'Home')
@section('content')
<div class="min-h-screen max-h-screen flex flex-col overflow-hidden bg-gray-100">
    <div class="bg-white border-b">
        <div class="flex justify-between container py-4 mx-auto">
            <a href="{{ url('/') }}" class="text-lg font-semibold">Blog.</a>

            @if (Route::has('login'))
                <div>
                    @auth
                        <a href="{{ url('/admin/dashboard') }}" class="text-sm text-gray-700 underline">Dashboard</a>
                    @else
                        <a href="{{ route('login') }}" class="text-sm text-gray-700 underline">Login</a>
        
                        @if (Route::has('register'))
                            <a href="{{ route('register') }}" class="ml-4 text-sm text-gray-700 underline">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
        </div>
    </div>

    <div class="flex-1 flex flex-col overflow-hidden container mx-auto">
        <div class="pt-8">
            <form method="POST" action="{{ route('search') }}" class="flex items-center">
                @csrf

                <input class="h-10 block w-full text-sm border px-3 py-2" name="search" />
    
                <button class="h-10 text-white font-semibold bg-gray-800 px-3 py-2"><i data-feather="search" width="16"></i></button>
            </form>
        </div>

        <div class="flex-1 flex overflow-hidden pt-8">
            <div class="w-3/4 overflow-y-auto">
                @foreach ($posts as $post)
                    <div class="flex bg-white border p-2 mb-4">
                        <div class="h-32 w-32 mr-2">
                            <img class="object-fill" src="{{ url($post->image . '/' . $post->id . '.jpg') }}" alt="{{ $post->title }}">
                        </div>
    
                        <div class="flex-1 flex flex-col">
                            <h4 class="text-md font-semibold">
                                {{ $post->title }}
                            </h4>
    
                            <p class="flex-1 text-sm font-medium">{{ $post->short_description }}</p>
    
                            <div class="flex justify-end">
                                <a href="{{ route('show', $post->slug) }}" class="text-white text-sm font-semibold bg-gray-800 px-3 py-2"> Read more</a>
                            </div>
                        </div>
                    </div>
                @endforeach

                {{ $posts->links('vendor.pagination.index') }}
            </div>

            <div class="w-1/4 pl-4">
                <div class="bg-white border p-2">
                    <h4 class="text-center text-md font-semibold">Category</h4>

                    <ul>
                        @foreach ($categories as $category)
                            <li class="mb-2">
                                <a href="{{ route('category', $category->slug) }}">{{ $category->name }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection