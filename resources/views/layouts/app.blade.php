<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Laravel') }} - @yield('title') </title>

        <!-- Fonts -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

        <!-- Styles -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://unpkg.com/feather-icons"></script>
        <script src="https://cdn.jsdelivr.net/npm/feather-icons/dist/feather.min.js"></script>
    </head>
    <body class="font-sans text-gray-800 antialiased">
        <div class="min-h-screen max-h-screen flex overflow-hidden bg-gray-100">
            <div class="w-1/6 flex flex-col bg-white border-r px-4 py-3">
                <a href="{{ url('/') }}" class="text-lg font-semibold p-4">Blog.</a>

                <div class="flex items-center bg-gray-100 border p-2 mx-2">
                    <div class="h-10 w-10 bg-gray-200 border mr-2"></div>

                    <div class="flex-1">
                        <label class="text-sm font-semibold">{{ Auth::user()->name }}</label>

                        <a href="{{ route('admin.profile') }}" class="block text-xs font-medium text-gray-600 underline">{{ __('Profile') }}</a>
                    </div>
                </div>

                <ul class="flex-1">
                    <li>
                        <a href="{{ route('admin.dashboard') }}" class="block text-sm font-medium px-4 py-2 mt-2">
                            {{ __('Dashboard') }}
                        </a>
                    </li>
                    
                    <li>
                        <a href="{{ route('admin.post') }}" class="block text-sm font-medium px-4 py-2 mt-2">
                            {{ __('Post') }}
                        </a>
                    </li>

                    <li>
                        <a href="{{ route('admin.category') }}" class="block text-sm font-medium px-4 py-2 mt-2">
                            {{ __('Category') }}
                        </a>
                    </li>
                </ul>

                <form method="POST" action="{{ route('logout') }}">
                    @csrf

                    <button class="w-full text-white text-sm font-semibold bg-gray-800 py-2">{{ __('Logout') }}</button>
                </form>
            </div>
            
            <main class="w-5/6">
                @yield('content')
            </main>
        </div>
    </body>

    <script>
        feather.replace()
    </script>
</html>
