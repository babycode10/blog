@extends('layouts.app')
@section('title', 'Post')
@section('content')
    <div class="h-full flex flex-col">
        <div class="px-8 pt-8">
            <form method="GET" action="{{ route('admin.post.search') }}" class="flex items-center">
                @csrf

                <input class="h-10 block w-full text-sm border px-3 py-2" name="search" />

                <button class="h-10 text-white font-semibold bg-gray-800 px-3 py-2"><i data-feather="search" width="16"></i></button>

                <a href="{{ route('admin.post.create') }}" class="h-10 text-white font-semibold bg-gray-800 px-3 py-2 ml-2"><i data-feather="plus" width="16"></i></a>
            </form>
        </div>

        <div class="flex-1 overflow-y-auto px-8 pt-8">
            <table class="w-full table-auto bg-white mb-4">
                <thead>
                    <tr>
                        <th class="w-16 border px-3 py-2">No.</th>
                        <th class="border px-3 py-2">Thumbnail</th>
                        <th class="text-left border px-3 py-2">Title</th>
                        <th class="text-left border px-3 py-2">Short Description</th>
                        <th class="w-40 border px-3 py-2">Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach ($posts as $key => $post)
                        <tr>
                            <td class="w-16 text-center border px-3 py-2">{{ $key + 1 }}</td>
                            <td class="w-40 border px-3 py-2">
                                <div class="flex justify-center">
                                    <img class="w-32" src="{{ url($post->thumbnail . '/' . $post->id . '.jpg') }}" alt="{{ $post->title }}">
                                </div>
                            </td>
                            <td class="border px-3 py-2">
                                {{ $post->title }}
                            </td>
                            <td class="border px-3 py-2">
                                {{ $post->short_description }}
                            </td>
                            <td class="w-40 border px-3 py-2">
                                <div class="flex items-center justify-center">
                                    <a href="{{ route('admin.post.edit', $post->id) }}" class="text-white text-sm font-semibold bg-gray-800 px-3 py-2 mr-2">
                                        <i data-feather="edit-2" width="16"></i>
                                    </a>
            
                                    <form method="POST" action="{{ route('admin.post.destroy', $post->id) }}">
                                        @csrf
                                        @method('delete')
            
                                        <button type="submit" class="text-white text-sm font-semibold bg-gray-800 px-3 py-2">
                                            <i data-feather="trash-2" width="16"></i>
                                        </button>
                                    </form>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $posts->links('vendor.pagination.index') }}
        </div>
    </div>
@endsection

