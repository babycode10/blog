@extends('layouts.app')
@section('title', 'Create Post')
@section('content')
    <div class="p-8">
        <div class="bg-white overflow-hidden border">
            <form method="POST" action="{{ route('admin.post.update', $post->id) }}" enctype="multipart/form-data">
                @csrf
                @method('put')

                <div class="px-4 pt-5">
                    <label class="text-sm" for="title">{{ __('Title') }}</label>

                    <input name="title" id="title" type="text"  class="block w-full text-sm border px-3 py-2 mt-1" value="{{ $post->title }}" />

                    @error('title')
                        <p class="text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                <div class="px-4 pt-5">
                    <label class="text-sm" for="short_description">{{ __('Short Description') }}</label>

                    <input name="short_description" id="short_description" type="text"  class="block w-full text-sm border px-3 py-2 mt-1" value="{{ $post->short_description }}" />

                    @error('short_description')
                        <p class="text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                <div class="px-4 pt-5">
                    <label class="text-sm" for="category_id">{{ __('Category') }}</label>

                    <select name="category_id" id="category_id" class="block w-full text-sm border px-3 py-2 mt-1" >
                        <option value="">Select Category</option>

                        @foreach ($categories as $category)
                            <option value="{{ $category->id }}" {{ $post->category_id == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                        @endforeach
                    </select>

                    @error('category_id')
                        <p class="text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                <div class="px-4 pt-5">
                    <label class="text-sm" for="content">{{ __('Content') }}</label>

                    <textarea name="content" id="content" class="block w-full text-sm border px-3 py-2 mt-1" >{{ $post->content }}</textarea>

                    @error('content')
                        <p class="text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                <div class="px-4 pt-5">
                    <label class="text-sm" for="image">{{ __('Image') }}</label>

                    <input name="image" id="image" type="file"  class="block w-full text-sm border px-3 py-2 mt-1" />

                    @error('image')
                        <p class="text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                <div class="flex items-center justify-end bg-gray-50 text-right border-t px-6 py-3 mt-5">
                    <button type="submit" class="text-white text-sm font-semibold bg-gray-800 px-3 py-2">
                        {{ __('Update') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
