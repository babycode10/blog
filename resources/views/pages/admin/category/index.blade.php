@extends('layouts.app')
@section('title', 'Category')
@section('content')
<div class="h-full flex flex-col">
    <div class="px-8 pt-8">
        <form method="GET" action="{{ route('admin.category.search') }}" class="flex items-center">
            @csrf

            <input class="h-10 block w-full text-sm border px-3 py-2" name="search" />

            <button class="h-10 text-white font-semibold bg-gray-800 px-3 py-2"><i data-feather="search" width="16"></i></button>

            <a href="{{ route('admin.category.create') }}" class="h-10 text-white font-semibold bg-gray-800 px-3 py-2 ml-2"><i data-feather="plus" width="16"></i></a>
        </form>
    </div>

    <div class="flex-1 overflow-y-auto px-8 pt-8">
        <table class="w-full table-auto bg-white mb-4">
            <thead>
                <tr>
                    <th class="w-16 border px-3 py-2">No.</th>
                    <th class="text-left border px-3 py-2">Name</th>
                    <th class="border px-3 py-2">Slug</th>
                    <th class="w-40 border px-3 py-2">Action</th>
                </tr>
            </thead>
            
            <tbody>
                @foreach ($categories as $key => $category)
                <tr>
                    <td class="text-center border px-3 py-2">{{ $key + 1 }}</td>
                    <td class="border px-3 py-2">{{ $category->name }}</td>
                    <td class="text-center border px-3 py-2">{{ $category->slug }}</td>
                    <td class="border px-3 py-2">
                        <div class="flex items-center justify-center">
                            <a href="{{ route('admin.category.edit', $category->id) }}" class="text-white text-sm font-semibold bg-gray-800 px-3 py-2 mr-2">
                                <i data-feather="edit-2" width="16"></i>
                            </a>
    
                            <form method="POST" action="{{ route('admin.category.destroy', $category->id) }}">
                                @csrf
                                @method('delete')
    
                                <button type="submit" class="text-white text-sm font-semibold bg-gray-800 px-3 py-2">
                                    <i data-feather="trash-2" width="16"></i>
                                </button>
                            </form>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>

        {{ $categories->links('vendor.pagination.index') }}
    </div>
</div>
@endsection

