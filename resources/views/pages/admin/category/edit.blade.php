@extends('layouts.app')
@section('title', 'Edit Category')
@section('content')
    <div class="p-8">
        <div class="bg-white overflow-hidden border">
            <form method="POST" action="{{ route('admin.category.update', $category->id) }}">
                @csrf
                @method('put')

                <div class="px-4 pt-5">
                    <label class="text-sm" for="name">{{ __('Name') }}</label>

                    <input name="name" id="name" type="text"  class="block w-full text-sm border px-3 py-2 mt-1" value="{{ $category->name }}" />

                    @error('name')
                        <p class="text-sm text-red-600">{{ $message }}</p>
                    @enderror
                </div>

                <div class="flex items-center justify-end bg-gray-50 text-right border-t px-6 py-3 mt-5">
                    <button type="submit" class="text-white text-sm font-semibold bg-gray-800 px-3 py-2">
                        {{ __('Update') }}
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
