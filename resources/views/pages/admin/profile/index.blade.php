@extends('layouts.app')
@section('title', 'Profile')
@section('content')
    <div class="p-8">
        <div class="grid grid-cols-3 gap-6">
            <div>
                <h3 class="text-md font-medium text-gray-900">{{ __('Update Password') }}</h3>
            
                <p class="mt-1 text-sm text-gray-600">
                    {{ __('Ensure your account is using a long, random password to stay secure.') }}
                </p>
            </div>

            <div class="col-span-2">
                <form method="POST" action="{{ route('admin.profile.update', $user->id) }}">
                    @csrf
                    @method('put')

                    <div class="overflow-hidden bg-white border">
                        <div class="px-4 pt-5">
                            <label class="text-sm" for="current_password">{{ __('Current Password') }}</label>

                            <input name="current_password" id="current_password" type="password" class="block w-full text-sm border px-3 py-2 mt-1" />

                            @error('current_password')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                
                        <div class="px-4 pt-5">
                            <label class="text-sm" for="password">{{ __('New Password') }}</label>

                            <input name="password" id="password" type="password" class="block w-full text-sm border px-3 py-2 mt-1" />

                            @error('password')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
                
                        <div class="px-4 pt-5">
                            <label class="text-sm" for="password_confirmation">{{ __('Confirm Password') }}</label>

                            <input name="password_confirmation" id="password_confirmation" type="password"  class="block w-full text-sm border px-3 py-2 mt-1" />

                            @error('password_confirmation')
                                <p class="text-sm text-red-600">{{ $message }}</p>
                            @enderror
                        </div>
        
                        <div class="flex items-center justify-end bg-gray-50 text-right border-t px-6 py-3 mt-5">
                            <button class="text-white text-sm font-semibold bg-gray-800 px-3 py-2">
                                {{ __('Save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection