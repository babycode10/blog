@extends('layouts.guest')
@section('title', 'Signup')
@section('content')
    <div class="min-h-screen flex items-center justify-center font-sans text-gray-900 antialiased bg-gray-100">
        <form class="bg-white overflow-hidden border w-1/4" method="POST" action="{{ route('register') }}">
            @csrf

            <!-- Name -->
            <div class="px-4 pt-5">
                <label class="text-sm" for="name">{{ __('Name') }}</label>

                <input name="name" id="name" type="text"  class="block w-full text-sm border px-3 py-2 mt-1" />

                @error('name')
                    <p class="text-sm text-red-600">{{ $message }}</p>
                @enderror
            </div>

            <!-- Email Address -->
            <div class="px-4 pt-5">
                <label class="text-sm" for="email">{{ __('Email') }}</label>

                <input name="email" id="email" type="email"  class="block w-full text-sm border px-3 py-2 mt-1" />

                @error('email')
                    <p class="text-sm text-red-600">{{ $message }}</p>
                @enderror
            </div>

            <!-- Password -->
            <div class="px-4 pt-5">
                <label class="text-sm" for="password">{{ __('Password') }}</label>

                <input name="password" id="password" type="password"  class="block w-full text-sm border px-3 py-2 mt-1" />

                @error('password')
                    <p class="text-sm text-red-600">{{ $message }}</p>
                @enderror
            </div>

            <!-- Confirm Password -->
            <div class="px-4 pt-5">
                <label class="text-sm" for="password_confirmation">{{ __('Confirm Password') }}</label>

                <input name="password_confirmation" id="password_confirmation" type="password"  class="block w-full text-sm border px-3 py-2 mt-1" />

                @error('password_confirmation')
                    <p class="text-sm text-red-600">{{ $message }}</p>
                @enderror
            </div>

            <!-- Remember Me -->
            <div class="px-4 pt-5">
                <label for="remember_me" class="flex items-center">
                    <input id="remember_me" type="checkbox" class="form-checkbox" name="remember">

                    <span class="ml-2 text-sm text-gray-600">{{ __('Remember me') }}</span>
                </label>
            </div>

            <div class="bg-gray-50 border-t mt-5">
                <div class="flex items-center justify-end px-4 py-3">
                    <button type="submit" class="text-white text-sm font-semibold bg-gray-800 px-3 py-2">
                        {{ __('Register') }}
                    </button>

                    <span class="text-xs text-center mx-2">
                        Or Signup with
                    </span>

                    <a href="{{ url('/login/github') }}" class="text-white text-sm font-semibold bg-gray-800 px-3 py-2">
                        <i data-feather="github" width="16"></i>
                    </a>
                </div>
            </div>
        </form>
    </div>
@endsection
